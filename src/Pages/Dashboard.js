import React from "react";
import { useState, useEffect } from "react";
import Dropdown from "../Component/Dropdown";
import { useNavigate } from "react-router-dom";

import { db } from "../firebase";
import {
  collection,
  addDoc,
  where,
  doc,
  query,
  onSnapshot,
  updateDoc,
} from "firebase/firestore";

const Dashboard = () => {
  const filterByData = [
    { id: 1, value: "New" },
    { id: 2, value: "Completed" },
    { id: 3, value: "Favourite" },
    { id: 4, value: "Deleted" },
  ];

  const markedListData = [
    { id: 1, value: "Completed" },
    { id: 2, value: "Favourite" },
    { id: 3, value: "Deleted" },
  ];

  const navigate = useNavigate();

  const [userDetails, setUserDetails] = useState(null);
  const [isFilterdDataOpen, setIsFilterdDataOpen] = useState(false);
  const [isMarkedList, setIsMarkedList] = useState(false);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [selectedData, setSelectedData] = useState("");
  const [selectedTodoId, setSelectedTodoId] = useState("");
  const [todos, setTodos] = useState([]);
  const [filterdTodos, setFilterdTodos] = useState([]);
  const [isDelected, setIsDelected] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const openFilterdDropdown = () => {
    setIsFilterdDataOpen(!isFilterdDataOpen);
  };
  const openMarkedList = (id) => {
    console.log(id);
    setSelectedTodoId(id);
    setIsMarkedList(!isMarkedList);
  };

  const handleFilterdData = (selectedData) => {
    // Handle the selected data in the parent component
    setIsDelected(true);
    console.log("Selected Data in Parent:", selectedData);
    let todosArray = todos.filter((todo) => todo.status === selectedData);
    console.log("todosArray", todosArray);
    setFilterdTodos(todosArray);
    setSelectedData(selectedData);
  };

  const handleMarkedListData = async (selectedData) => {
    // Handle the selected data in the parent component
    console.log("Selected Data in Parent:", selectedData, selectedTodoId);
    await updateDoc(doc(db, "todos", selectedTodoId), { status: selectedData });
    // setSelectedData(selectedData);
  };

  const handleSearch = (event) => {
    const searchValue = event.target.value.toLowerCase();
    setSearchTerm(searchValue);

    const filteredResults = todos.filter(
      (item) =>
        item.titleDb.toLowerCase().includes(searchValue) ||
        item.descriptionDb.toLowerCase().includes(searchValue)
    );

    setFilterdTodos(filteredResults);
  };

  const addTodoList = async (e) => {
    e.preventDefault();
    if (title !== "" && description !== "") {
      await addDoc(collection(db, "todos"), {
        userEmail: userDetails.email,
        titleDb: title,
        descriptionDb: description,
        status: "New",
      });
      setTitle("");
      setDescription("");
    }
  };

  const logout = () => {
    localStorage.removeItem("userDetails");
    navigate("/");
  };

  useEffect(() => {
    const details = localStorage.getItem("userDetails");
    setUserDetails(JSON.parse(details));

    // const q = query(collection(db, "todos"));
    const q = query(
      collection(db, "todos"),
      where("userEmail", "==", JSON.parse(details).email)
    );
    const unsub = onSnapshot(q, (querySnapshot) => {
      let todosArray = [];
      querySnapshot.forEach((doc) => {
        todosArray.push({ ...doc.data(), id: doc.id });
      });
      setTodos(todosArray);
      setFilterdTodos(todosArray);
      console.log("Data", todosArray);
    });
    return () => unsub();
  }, []);

  return (
    <div>
      <div className="p-12 h-dvh block md:flex justify-around  ">
        <div className="md:p-6 py-6 w-full md:w-1/2  md:border-r-2 ">
          <div className="flex justify-between items-center">
            <div className="w-12 h-12  bg-[url('./Assets/Group.svg')] bg-contain bg-no-repeat"></div>
            <div className="flex ">
              <div className="font-bold">{userDetails?.displayName}</div>
              <div
                onClick={logout}
                title="Logout"
                className="w-6 h-6 ml-3 cursor-pointer  bg-[url('./Assets/logout-2-svgrepo-com.svg')] bg-contain bg-no-repeat"
              ></div>
            </div>
          </div>
          <div className="mt-8 md:pr-10  flex justify-center">
            <div>
              <div className="font-bold flex justify-center">TODO</div>
              <div className="mt-2 text-center">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aliquet at eleifend feugiat vitae faucibus nibh dolor dui.
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aliquet at eleifend feugiat vitae faucibus nibh dolor dui.
                </p>
              </div>
              <div className="flex flex-col justify-center items-center">
                <div className="mt-6 flex justify-center">
                  <input
                    className="p-1 border-2 shadow-lg outline-none"
                    type="text"
                    name="titile"
                    placeholder="Title"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </div>
                <div className="mt-3 flex justify-center">
                  <input
                    className="p-1 border-2 shadow-lg outline-none"
                    type="text"
                    name="description"
                    placeholder="Description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </div>
                <div
                  onClick={addTodoList}
                  className="mt-5 w-2/6 flex justify-center items-center text-white bg-buttonBackgroundColour cursor-pointer rounded-md"
                >
                  <div>Add</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="md:p-6 md:px-12 md:w-1/2 ">
          <div className="font-bold text-center">TODO LIST</div>
          <div className="mt-10 flex justify-between">
            <div className="p-0.5  flex justify-between border-2 shadow-lg">
              <div>
                <input
                  className=" outline-none w-24 md:w-auto"
                  type="text"
                  name="search"
                  placeholder="Search"
                  value={searchTerm}
                  onChange={handleSearch}
                />
              </div>
              <div className="w-6 h-6  bg-[url('./Assets/Search.svg')] bg-contain bg-no-repeat"></div>
            </div>
            <div
              className="p-0.5 px-2 w-32 flex justify-between items-center border-2 shadow-lg cursor-pointer"
              onClick={openFilterdDropdown}
            >
              {selectedData === "" ? (
                <div className="text-nowrap">Filter By</div>
              ) : (
                <div
                  className="text-nowrap"
                  dangerouslySetInnerHTML={{ __html: selectedData }}
                ></div>
              )}
              <div className="w-3 h-3  bg-[url('./Assets/icon.svg')] bg-contain bg-no-repeat"></div>
              {isFilterdDataOpen && (
                <Dropdown data={filterByData} onSelect={handleFilterdData} />
              )}
            </div>
          </div>
          <div className="mt-16 h-56 overflow-y-scroll">
            {filterdTodos.map((todo, index) => {
              if (todo.status !== "Deleted") {
                return (
                  <div
                    key={index}
                    className="py-4 border-b-2  flex justify-between items-center"
                  >
                    <div>
                      <div className="font-bold">{todo.titleDb}</div>
                      <div className="text-sm text-gray-400">
                        <p title={todo.descriptionDb}>
                          {todo.descriptionDb.slice(0, 20)}...
                        </p>
                      </div>
                    </div>
                    <div
                      onClick={() => openMarkedList(todo.id)}
                      className="w-4 h-4 cursor-pointer bg-[url('./Assets/More.svg')] bg-contain bg-no-repeat"
                    >
                      {isMarkedList && (
                        <div className="absolute right-44 top-32">
                          <Dropdown
                            data={markedListData}
                            todoId={todo.id}
                            onSelect={handleMarkedListData}
                          />
                        </div>
                      )}
                    </div>
                  </div>
                );
              } else if (todo.status === "Deleted" && isDelected) {
                return (
                  <div
                    key={index}
                    className="py-4 border-b-2  flex justify-between items-center"
                  >
                    <div>
                      <div className="font-bold">{todo.titleDb}</div>
                      <div className="text-sm text-gray-400">
                        <p title={todo.descriptionDb}>
                          {todo.descriptionDb.slice(0, 20)}...
                        </p>
                      </div>
                    </div>
                    <div
                      onClick={() => openMarkedList(todo.id)}
                      className="w-4 h-4 cursor-pointer bg-[url('./Assets/More.svg')] bg-contain bg-no-repeat"
                    >
                      {isMarkedList && (
                        <div className="absolute right-0 bottom-0 md:right-44 md:top-32">
                          <Dropdown
                            data={markedListData}
                            todoId={todo.id}
                            onSelect={handleMarkedListData}
                          />
                        </div>
                      )}
                    </div>
                  </div>
                );
              }
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
