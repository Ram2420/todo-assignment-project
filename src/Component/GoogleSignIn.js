import { React } from "react";
import { signInWithPopup } from "firebase/auth";
import { provider, auth } from "../firebase"; // Import auth from firebase.js
import { GoogleButton } from "react-google-button";
import { useNavigate } from "react-router-dom";

const GoogleSignIn = () => {
  const navigate = useNavigate();

  const signInWithGoogle = () => {
    signInWithPopup(auth, provider)
      .then((res) => {
        // Signed-in user information (res.user)

        localStorage.setItem("userDetails", JSON.stringify(res.user));
        navigate("/dashboard");
        console.log(res.user);
      })
      .catch((error) => {
        // Handle errors
        console.error(error);
      });
  };

  return (
    <div className="z-50">
      {/* <button onClick={signInWithGoogle}>Sign in with Google</button> */}
      <GoogleButton onClick={signInWithGoogle} />
    </div>
  );
};

export default GoogleSignIn;
