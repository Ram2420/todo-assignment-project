import React from "react";

const Dropdown = (props) => {
  const { data, todoId } = props;

  const handleSelection = (value, id) => {
    // console.log("Value", value);
    // Pass the selected data to the parent
    props.onSelect(value, todoId);
  };

  return (
    <div className="mt-40  w-auto shadow-xl top-80 md:top-0  absolute  bg-white">
      {data.map((item) => (
        <div
          className="mt-2 py-0.5 px-6 cursor-pointer hover:bg-gray-400"
          key={item.id}
        >
          <div
            onClick={(e) => {
              e.preventDefault();
              handleSelection(item.value, todoId);
            }}
          >
            {item.value}
          </div>
        </div>
      ))}
    </div>
  );
};

export default Dropdown;
