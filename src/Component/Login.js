import React from "react";
import GoogleSignIn from "./GoogleSignIn";

const Login = () => {
  return (
    <div className="w-full h-full">
      <div className="w-24 md:w-32 h-32 top-0 left-3/4  absolute  bg-[url('./Assets/chandelier-with-green-round-lampshade.png')] bg-contain bg-no-repeat"></div>
      <div className="m-6 w-56 h-32 md:w-44 md:h-44 lg:w-56 lg:h-56 xl:w-60 xl:h-60 absolute bottom-16 -right-5 md:bottom-24 md:right-24 lg:bottom-28 lg:right-16 xl:bottom-40 xl:right-32  z-40  bg-[url('./Assets/Rectangle.png')] bg-contain bg-no-repeat"></div>
      <div className="w-full md:w-1/2 h-3/6 md:h-full bottom-0  right-0  absolute   bg-[url('./Assets/bg-image.svg')] bg-contain  bg-right-bottom bg-no-repeat"></div>
      <div>
        <div className="m-6 w-12 h-12   bg-[url('./Assets/Group.svg')] bg-contain bg-no-repeat"></div>
        <div className="p-6 w-full md:w-1/2 flex justify-center">
          <div>
            <div className="md:pt-10 flex justify-center font-bold">LOGIN</div>
            <div className="pt-5 md:pt-10 text-center text-gray-500">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquet
                at eleifend feugiat vitae faucibus nibh dolor dui. Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Aliquet at eleifend
                feugiat vitae faucibus nibh dolor dui.
              </p>
            </div>
            <div className="pt-10 flex justify-center">
              <GoogleSignIn />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
