import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAIDRXmYI6tFyzRDPISiSdTKbs4edGb4Pk",
  authDomain: "todo-25df3.firebaseapp.com",
  projectId: "todo-25df3",
  storageBucket: "todo-25df3.appspot.com",
  messagingSenderId: "517485072047",
  appId: "1:517485072047:web:c0d7bea0698475ddfb8e5a",
  measurementId: "G-JMBVQJY2RB",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app); // Initialize auth here
const db = getFirestore(app);
const provider = new GoogleAuthProvider();

export { auth, provider, db };
