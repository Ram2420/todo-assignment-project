import "./App.css";
import Login from "./Component/Login";
import { useEffect } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Dashboard from "./Pages/Dashboard";
import { auth } from "./firebase";
// import PrivateRoute from "./PrivateRoute";

function App() {
  // const [isLoggedIn, setIsLoggedIn] = useState(false);
  const user = auth?.currentUser;

  const PrivateRoute = ({ children }) => {
    const details = localStorage.getItem("userDetails");
    console.log("user", JSON.parse(details));
    try {
      return JSON.parse(details) ? children : <Navigate to="/" />;
    } catch (error) {
      console.error("Error parsing JSON:", error);
      // Handle the error or redirect to a default route
      return <Navigate to="/" />;
    }
  };

  useEffect(() => {
    //   var storedData = localStorage.getItem("userDetails");
    //   if (storedData) {
    //     setIsLoggedIn(true);
    //   }
  }, []);

  return (
    <div className="w-full  ">
      <div>
        {/* <Login /> */}
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route
              path="/dashboard"
              element={
                <PrivateRoute>
                  <Dashboard />
                </PrivateRoute>
              }
            ></Route>
            <Route path="*" element={<Login />} />
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
